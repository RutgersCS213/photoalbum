# README #

Clone the project in a file path that doesn't have any spaces so that the fonts load. Apparently, if the file path has a space in it, css replaces it with '%20' and that makes it so the fonts don't load for the app.

### Photo Album ###

* Multiple users can manage their own albums and tag their photos.

### Users/Passwords ###

- username: admin ; password: password
- username: sampleUser ; password: password
- username: sampleUser2 ; password: password

You can also register new users from login screen which you can then log into and create albums.

Sample images in folder "sample_images" in resources package.