package database;

import java.io.Serializable;

/**
 * Created by Ragen on 11/21/16.
 */
public class AlbumList implements Serializable {

    public String AlbumName;
    public String AlbumPath;
    public AlbumList next;

    public AlbumList() {
        this.next = null;
    }

    public AlbumList(String AlbumName, String AlbumPath) {
        this.AlbumName = AlbumName;
        this.AlbumPath = AlbumPath;
    }

}
