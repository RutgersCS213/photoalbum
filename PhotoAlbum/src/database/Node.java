package database;

/**
 * Created by Ragen on 11/15/16.
 */
public class Node {

    private String username, password;
    int id = 0;
    public Node next;

    public Node() {
        this.next = null;
    }

    public Node(String username, String password) {
        this.username = username;
        this.password = password;
        this.id = id++;
    }

    public int getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }




}
