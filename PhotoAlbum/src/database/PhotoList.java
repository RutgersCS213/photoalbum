package database;

import java.io.Serializable;

public class PhotoList implements Serializable {

    public String PhotoName;
    public String PhotoPath;
    public PhotoList next;
    public String[] tags;
    public String caption;

    public String[] masterTags;

    public PhotoList() {
        this.next = null;
    }

    public PhotoList(String PhotoName, String PhotoPath, String[] tags, String caption) {
        this.PhotoName = PhotoName;
        this.PhotoPath = PhotoPath;
        this.tags = tags;
        this.caption = caption;



    }

}
