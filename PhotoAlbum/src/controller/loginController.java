package controller;

import com.oracle.javafx.jmx.json.JSONWriter;
import database.Node;
import database.user;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import view.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;


import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

import static java.lang.System.exit;

/**
 * Controller for the login view of the app. Handles all login and register events.
 */
public class loginController extends Main {

    public String currentUser;

    @FXML
    private TextField loginTextField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button loginButton;

    @FXML
    private Button closeButton;

    @FXML
    private Button registerButton;

    //Global variable for the first value in the node.
    public Node initNode = new Node();
    Node lastNode = null;


    /**
     * Handles button click events.
     * @param loginPage
     */
    @FXML
    @Override
    public void start(Stage loginPage) {


        //Loads up the database into system.
        setinitNode();

        //System.out.println(initNode.getUsername() + " : " + initNode.getPassword());


        //Lambda expression to do the onclick event.
        loginButton.setOnAction(e -> loginEvent(e, loginPage));

        closeButton.setOnAction(event -> closeEvent());

        registerButton.setOnAction(e -> registerEvent());
    }


    /**
     * Handles the event of the close button being clicked.
     */
    private void closeEvent() {
        System.out.println("Exit SUCCESS");
        exit(0);
    }


    /**
     * Checks if there is a name with the same user and gives error if there is. Otherwise, it adds the new user password
     * entry in the database.
     */
    private void registerEvent() {

        boolean didPassEmptyCheck = checkEmpty();

        if (didPassEmptyCheck == true) {
            Node temp = initNode;

            while (temp != null) {
                if (temp.getUsername().compareTo(loginTextField.getText()) == 0) {
                    //Username already exists. try again.
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Username already exists.");
                    alert.setHeaderText("The username you entered already exists.");
                    alert.setContentText("Try again with a valid username.");
                    alert.showAndWait();

                    passwordField.setText("");
                    loginTextField.setText("");
                    break;
                }

                lastNode = temp;
                temp = temp.next;


            }

            if (loginTextField.getText().isEmpty()) {
                //Do nothing
            } else {
                Node newUser = new Node(loginTextField.getText(), passwordField.getText());
                lastNode.next = newUser;
                System.out.println("temp is: " + lastNode.getUsername());
                lastNode = newUser;
                user.addToFile(newUser);
                System.out.println("Registered new user");

                File path = new File("Users/");

                //Creates a directory where data for this user is saved.
                new File(path.getAbsolutePath() + "/" + lastNode.getUsername()+"/").mkdirs();
                System.out.println("New folder created for user at : " + path.getAbsolutePath()+"/");
            }

            //initNode.next = new Node("admin", "password");
            //Node tempNode = initNode.next;


        }

    }


    /**
     * Handles event of the login button being clicked. Checks if it is a valid input or not.
     * If it is, checks username and opens admin view or user album view as required.
     */
    private void loginEvent(ActionEvent event, Stage loginPage) {

        //value to check if we need to give an error or continue on to the program.
        boolean didPassEmptyCheck = checkEmpty();

        //Passed empty field check.
        if (didPassEmptyCheck == true) {
            if (loginTextField.getText().compareTo("admin") == 0) {
                Node temp = initNode;

                while (temp != null) {
                    if (temp.getUsername().compareTo(loginTextField.getText()) == 0) {
                        //Found username. Check password.
                        if (temp.getPassword().compareTo(passwordField.getText()) == 0) {
                            //Password matches
                            System.out.println("SUCCESS: username password matches database. Going into admin view.");

                            //Creates new admin page controller
                            //adminPageController adminPage = new adminPageController();
                            //loginPage.close();
                            //use .setScene instead.
                            try {
                                openAdminPage(event, loginPage);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            /*
                            try {
                                adminPage.setAdminPage(event, loginPage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/


                        } else {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Invalid password");
                            alert.setHeaderText("The password you entered does not match the username.");
                            alert.setContentText("Try again with a valid password.");
                            alert.showAndWait();

                            passwordField.setText("");
                        }

                        passwordField.setText("");
                        loginTextField.setText("");
                        break;
                    }

                    lastNode = temp;
                    temp = temp.next;
                }

            } else {
                Node temp = initNode;
                boolean check = false;

                while (temp != null) {
                    if (temp.getUsername().compareTo(loginTextField.getText()) == 0) {
                        //Username already exists.
                        if(temp.getPassword().compareTo(passwordField.getText()) == 0) {
                            //matched password and username. Success. Move on to photo album.
                            System.out.println("SUCCESS: username password matches database.");
                            check = true;
                            //Do some stuff to go to PHOTO ALBUM VIEW HERE.
                            try {
                                openAlbumPage(temp.getUsername(), loginPage);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Invalid password");
                            alert.setHeaderText("The password you entered does not match the username.");
                            alert.setContentText("Try again with a valid password.");
                            alert.showAndWait();
                            check = true;
                        }

                        passwordField.setText("");
                        break;
                    }

                    lastNode = temp;
                    temp = temp.next;

                }

                if(check == false) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Invalid username");
                    alert.setHeaderText("The username you entered does not match any users in the database.");
                    alert.setContentText("Register your account to access your photo album.");
                    alert.showAndWait();
                }

            }

        }
    }


    /**
     * Method to open AdminPage view.
     * @param event
     * @param parentStage
     * @throws IOException
     */
    public void openAdminPage(ActionEvent event, Stage parentStage) throws IOException {
        FXMLLoader adminPage = new FXMLLoader(getClass().getResource("/resources/adminPage.fxml"));
        Parent root = (Parent) adminPage.load();
        adminPageController ad = new adminPageController();
        ad.start(parentStage);

        Scene adminPageScene = new Scene(root);

        parentStage.setTitle("Admin View");
        parentStage.setScene(adminPageScene);
        parentStage.show();

    }


    /**
     * Method to open albumPage view.
     * @param username
     * @param parentStage
     * @throws IOException
     */
    public void openAlbumPage(String username, Stage parentStage) throws IOException {
        FXMLLoader albumPage = new FXMLLoader(getClass().getResource("/resources/albumPage.fxml"));
        Parent albumParent = (Parent)albumPage.load();
        Scene albumPageScene = new Scene(albumParent);

        albumViewController a = new albumViewController();

        currentUser = username;

        a.setCurrentUser(username);

        a.start(parentStage);
        //parentStage.hide();
        parentStage.setTitle("Album View");
        parentStage.setScene(albumPageScene);
    }


    /**
     * private method that is used to check if the text fields are left empty or not when login or register button is pressed.
     * @return boolean value
     */
    private boolean checkEmpty() {

        //value to check if we need to give an error or continue on to the program.
        boolean didPassEmptyCheck = true;
        if(loginTextField.getText().isEmpty()) {
            Alert invalidLogin = new Alert(Alert.AlertType.WARNING);
            invalidLogin.setTitle("Empty Field");
            invalidLogin.setHeaderText("Empty username field.");
            invalidLogin.setContentText("Try again with a valid username.");

            //Styling the popup window.
            /*DialogPane dPane = invalidLogin.getDialogPane();
            dPane.setStyle("-fx-background-color: #e95c64;");
            dPane.getStylesheets().add(getClass().getResource("/resources/myDialogs.css").toExternalForm());
            dPane.getStyleClass().add("myDialog");*/

            invalidLogin.showAndWait();
            return didPassEmptyCheck = false;
        } else if(passwordField.getText().isEmpty()) {
            Alert invalidPassword = new Alert(Alert.AlertType.WARNING);
            invalidPassword.setTitle("Empty Field");
            invalidPassword.setHeaderText("Empty password field.");
            invalidPassword.setContentText("Try again with a valid password.");
            invalidPassword.showAndWait();
            return didPassEmptyCheck = false;
        }

        return true;


    }

    /**
     * loads database to initNode.
     */
    protected void setinitNode() {

        int i = 0;
        String line = null;
        Node prev = null;
        try {
            FileReader file = new FileReader("src/database/database.txt");

            BufferedReader br = new BufferedReader(file);

            while((line = br.readLine()) != null) {

                //Gets username from file
                String username = line.substring(10, line.indexOf(';'));
                line = line.substring(line.indexOf(';')+1);

                //gets password from file
                String password = line.substring(11, line.indexOf(';'));


                //creates a new node with username and pass
                Node temp = new Node(username, password);
                //sets next of this new node to null
                temp.next = null;

                if(i>0) {
                    //sets previous node's next to the new node that has been created.
                    prev.next = temp;
                }

                //sets previous to the current node for the next iteration.
                prev = temp;


                //set initialNode to the first value we load in.
                if(i == 0) {
                    prev = temp;
                    initNode = temp;
                    i++;
                }

            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }





    }


}