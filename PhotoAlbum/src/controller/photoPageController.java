package controller;

import java.io.Serializable;

import static java.lang.System.exit;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import view.Main;

/**
 * Created by Ragen on 11/20/16.
 *
 * Controller for photos page. Handles all events that occur in photos view.
 */
public class photoPageController extends albumViewController implements Serializable{

	static Stage st= new Stage();

	public static String albumPath;

	public void setAlbumPath(String path) {
		albumPath = path;
	}


	@FXML
	private GridPane photoGrid;

	@FXML
	private Button addPhotoButton;

	@FXML
	private Button addTagButton;

	@FXML
	private Button deleteButton;

	@FXML
	private Button fullscreenButton;

	@FXML
	private TextField captionText;

	@FXML
	private TextField tagsText;

	@FXML
	private TextField searchText;

	@FXML
	private Button copyButton;

	@FXML
	private Button moveButton;

	@FXML
	private Button logoutButton;

	@FXML
	private Button searchButton;

	@FXML
	private Button slideshowButton;

	@FXML
	private Button editButton;

	@FXML
	private Button doneButton;

	@FXML
	protected void logoutButtonEvent(){
		signOutAction(st);
	}

	@FXML
	protected void doneButtonEvent(){
		doneAction();
	}

	@FXML
    @Override
    public void start(Stage loginPage) {

        st = loginPage;
        System.out.println("Start method in photoPageController " + currentUser);
        //Load photos in the album.
       // loadPhotos();
    }

	/**
     * Handles action event for signOutAction.
     */
	@FXML
    protected void signOutAction(Stage loginPage) {
        //return to login view.
     System.out.println("Signout event");
/*
        Main main = new Main();
        try {
			main.start(st);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        exit(0);

    }
	protected void doneAction(){
		System.out.println("PhotoPage Done Action");
		exit(0);
	}

}
