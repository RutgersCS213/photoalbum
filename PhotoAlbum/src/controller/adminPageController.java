package controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;
import view.Main;


import java.io.IOException;
import java.util.Optional;

import database.Node;
import database.user;

import static java.lang.System.exit;

/**
 * Created by Ragen on 11/15/16.
 */
public class adminPageController extends loginController {

    private Scene scene;
    static Stage st = new Stage();

    private static double xOffset = 0;
    private static double yOffset = 0;

    @FXML
   public ListView<String> listView = new ListView<String>();


    @FXML
    private static Button addButton;

    @FXML
    private static Button delButton;

    @FXML
    private static Button quitButton;

    @FXML
    private static Button signOutButton;

    @FXML
    TextField userName;

    @FXML
    PasswordField password;

    @FXML protected void handleSignoutEvent() {
        signOutAction(st);
    }

    @FXML protected void safeExit() {
        quitButtonAction();
    }

    @FXML protected void addButtonEvent() {
    	addUserAction();
    }

    @FXML protected void deleteButtonEvent() {
    	//deleteUserAction();
    }


    private ObservableList<String> obsList;

    /**
     * Main controller for admin page.
     */

    @Override
    public void start(Stage loginPage) {
    	st = loginPage;


    	obsList = FXCollections.observableArrayList();
    	setinitNode();

    	Node curr = initNode;
    	System.out.println(initNode.getUsername());
        Node last = curr;

        while(curr!=null){
        	last = curr;

       	 obsList.add(curr.getUsername());

        	curr = curr.next;
        }
        System.out.println(obsList);
        listView.setItems(null);
       listView.setItems(obsList);
       System.out.println(listView.getItems());
       try {

		setAdminPage(loginPage);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

    }


    /**
     * Quit button for admin page.
     */
    public static void quitButtonAction() {
        System.out.println("Admin quit action");
        exit(0);
    }

    /**
     * Handles action event for signOutAction.
     */
    public static void signOutAction(Stage loginPage) {
        //return to login view.
        System.out.println("Signout event");

        Main main = new Main();
        try {
			main.start(st);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }

    /**
     * Handles action event for addUserAction.
     */
    public void addUserAction(){
    	String userText = userName.getText();
    	String passwordText = password.getText();
    	setinitNode();
    	Node tmp = initNode;

    	if(userText.compareTo("")==0 || passwordText.compareTo("")==0){

    		System.out.println("Error. Could not add user. Either Username or Password was left empty.");

    		Alert alert = new Alert(AlertType.ERROR);
    		alert.setTitle("Error");
    		alert.setHeaderText("Error Adding User");
    		alert.setContentText("Make sure that both Username and Password fields are not left empty when adding a user.");

    		alert.showAndWait();
    	}
    	else{

    		while(tmp!=null){

    			if(userText.compareTo(tmp.getUsername())==0){
    				System.out.println("Error. Could not add user. Username already exists.");

    				Alert alert = new Alert(AlertType.ERROR);
    				alert.setTitle("Error");
    				alert.setHeaderText("Error Adding User");
    				alert.setContentText("Username is already in use. Please try a different username.");

    				alert.showAndWait();

    				return;
    			}
    			tmp=tmp.next;
    		}
    		Node userNode=new Node(userText, passwordText);
    		user.addToFile(userNode);
    		setinitNode();
    		System.out.println("User added successfully.");
    	}
    }



    /**
     * Handles action event for deleteUserAction.
     */
    public static void deleteUserAction(){

    }

    /**
     * Handles everything relating to admin page.
     * @throws Exception
     */
    public void setAdminPage(Stage loginPage) throws Exception{
        //Creates new window
        Parent root;
        try {

        	FXMLLoader adminPage = new FXMLLoader(getClass().getResource("/resources/adminPage.fxml"));
            root = (Parent) adminPage.load();

            Scene adminPageScene = new Scene(root);


            loginPage.setTitle("Admin View");
            loginPage.setScene(adminPageScene);
            loginPage.show();
            //Moves the window with mouse if you click and drag from anywhere on the window
            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                }
            });

            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                  loginPage.setX(event.getScreenX() - xOffset);
                  loginPage.setY(event.getScreenY() - yOffset);
                }
            });

        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }


}

