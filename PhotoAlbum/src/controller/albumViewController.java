package controller;

import database.AlbumList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import view.Main;

import java.io.*;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Ragen on 11/17/16.
 * Handles album view events that occur including creating new album, opening albums renames etc.
 */
public class albumViewController extends loginController implements Serializable {

    private Stage loginStage;

    @FXML
    private GridPane albumGrid;

    @FXML
    private TextField albumNameTextField;

    @FXML
    private TextField searchTextField;

    @FXML
    private Button createAlbumFromSearchButton;

    @FXML
    private Button openButton;

    @FXML
    private Button editButton;

    @FXML
    private Button deleteButton;

    @FXML
    private Button newAlbumButton;

    @FXML
    private Button logoutButton;

    @FXML
    private DatePicker dateToPicker;

    @FXML
    private DatePicker dateFromPicker;

    @FXML
    private ListView albumListView;


    public static String currentUser;

    public static final ObservableList<String> names = FXCollections.observableArrayList();;

    public static AlbumList obj;
    public static AlbumList currentNode = obj;

    private static String albumNameToChange;


    /**
     * sets current user as the username to find the folder and load the list of albums.
     * @param username
     */
    public void setCurrentUser(String username) {
        this.currentUser = username;
        System.out.println(this.currentUser);
    }


    /**
     * If search result works, creates new album with the photos.
     */
    @FXML
    protected void createAlbumEvent() {
        //Create new album folder.
        newAlbumButtonEvent();

        //Copy searched photos to newly created directory. Can only get this from photoPageController..
    }

    /**
     * Button search. Will be used to search for photos with specific tags.
     */
    @FXML
    protected void searchButtonEvent() {
        //Search for the tagged photos
    }

    /**
     * Enables edit mode for the album. Can edit folder name when editButtonOnAction is called
     */
    @FXML
    protected void enableEdit() {
        searchTextField.setDisable(true);
        createAlbumFromSearchButton.setDisable(true);
        openButton.setDisable(true);
        newAlbumButton.setDisable(true);
        logoutButton.setDisable(true);
        dateToPicker.setDisable(true);
        dateFromPicker.setDisable(true);
        deleteButton.setDisable(true);
        editButton.setDisable(false);
        albumNameToChange = albumNameTextField.getText();

    }

    /**
     * Renames album to the specific name.
     */
    @FXML
    protected void editButtonOnAction() {
        //Change file directory name to new value. Enable all buttons and disable editTextBox.
        if(albumNameTextField.getText().compareTo(albumNameToChange) == 0 || albumListView.getItems().size() == 0) {
            //Name stayed the same or there are no items in the list to edit. No need to do anything.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("No Album Selected");
            alert.setContentText("Nothing to edit. No album selected.");
            searchTextField.setDisable(false);
            createAlbumFromSearchButton.setDisable(false);
            openButton.setDisable(false);
            newAlbumButton.setDisable(false);
            logoutButton.setDisable(false);
            dateToPicker.setDisable(false);
            dateFromPicker.setDisable(false);
            deleteButton.setDisable(false);
            editButton.setDisable(true);
        } else {
            //Album selected is valid. Edit the name.
            //Find albumNameToChange file path

            AlbumList deserializeObject;
            AlbumList addToSerialized = new AlbumList();
            AlbumList first = addToSerialized;
            int j = 0;
            do {
                deserializeObject = deserialize();
                if(deserializeObject.AlbumName.compareTo(albumNameToChange) != 0) {
                    addToSerialized.AlbumName = deserializeObject.AlbumName;
                    addToSerialized.AlbumPath = deserializeObject.AlbumPath;
                    addToSerialized.next = new AlbumList();
                    System.out.println(addToSerialized.AlbumName);
                    addToSerialized = addToSerialized.next;
                    j++;
                }
            } while (deserializeObject.AlbumName.compareTo(albumNameToChange) != 0 || j < 50);

            //Path of the album
            String path = deserializeObject.AlbumPath;

            File dir = new File(path);
            if (!dir.isDirectory()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Dir not found");
                alert.setContentText("The file path to the folder does not exist. Check if the folder exists.");
            }

            File newDir = new File(dir.getParent() + "/" + albumNameTextField.getText());

            addToSerialized.AlbumName = albumNameTextField.getText();
            addToSerialized.AlbumPath = dir.getParent() + "/" + albumNameTextField.getText();

            dir.renameTo(newDir);
            int i = albumListView.getSelectionModel().getSelectedIndex();
            names.remove(i);
            names.add(albumNameTextField.getText());
            albumListView.setItems(names);

            //Finished renaming. Add the updated list to serialized object.
            serialize(first);

            searchTextField.setDisable(false);
            createAlbumFromSearchButton.setDisable(false);
            openButton.setDisable(false);
            newAlbumButton.setDisable(false);
            logoutButton.setDisable(false);
            dateToPicker.setDisable(false);
            dateFromPicker.setDisable(false);
            deleteButton.setDisable(false);
            editButton.setDisable(true);
        }
    }

    /**
     * Search function to search for photos with specific tags.
     */
    @FXML
    protected void searchTagOnAction() {
        //disable date and time as long as the textfield has a char in it.
        if(searchTextField.getLength() > 0) {
            dateFromPicker.setDisable(true);
            dateToPicker.setDisable(true);
        }

        if(searchTextField.getLength() == 0) {
            dateFromPicker.setDisable(false);
            dateToPicker.setDisable(false);
        }
    }

    /**
     * Event to select date for search
     */
    @FXML
    protected void dateFromPickerMouse() {
        if(dateFromPicker.getAccessibleText().length() > 0) {
            searchTextField.setDisable(true);
        }
    }

    /**
     * Event to enable text field tag search if date is deleted
     */
    @FXML
    protected void dateFromPickerKeyboard() {
        if(dateFromPicker.getAccessibleText().length() == 0) {
            searchTextField.setDisable(false);
        }
    }

    /**
     * Event to select date for search
     */
    @FXML
    protected void dateToPickerMouse() {
        if(dateFromPicker.getAccessibleText().length() > 0) {
            searchTextField.setDisable(true);
        }
    }

    /**
     * Event to enable text field tag search if date is deleted
     */

    @FXML
    protected void dateToPickerKeyboard() {
        if(dateFromPicker.getAccessibleText().length() == 0) {
            searchTextField.setDisable(false);
        }
    }

    /**
     * sets editable text on the right with the name of the album.
     */
    @FXML
    protected void listViewMouseClicked() {
        if(albumListView.getItems().size() == 0) {
            albumNameTextField.setText("");
        } else {
            albumNameTextField.setText(albumListView.getSelectionModel().getSelectedItem().toString());
        }

    }



    /**
     * Creates a new empty Album.
     */
    @FXML
    protected void newAlbumButtonEvent() {
        //Handle Album Event. Open directory chooser to create new album.
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDir = directoryChooser.showDialog(newAlbumButton.getScene().getWindow());


        //Save file path of the selected album in the data file and show it on list.
        if(selectedDir == null) {
            System.out.println("ERROR: No directory selected.");
        } else {
            System.out.println("Selected directory: "+selectedDir.getAbsolutePath());

            AlbumList obj = new AlbumList(selectedDir.getName(), selectedDir.getAbsolutePath());

            System.out.println("CurrentUser: " + currentUser);
            //Save new album path in serialized object.
            File yourFile = new File("Users/"+this.currentUser+"/"+this.currentUser+".data");
            try{
                yourFile.createNewFile(); // if file already exists will do nothing
                FileOutputStream oFile = new FileOutputStream(yourFile, false);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                FileOutputStream fileOut =
                        new FileOutputStream("Users/"+currentUser+"/"+this.currentUser+".data");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(obj);
                out.close();
                fileOut.close();
                System.out.printf("Serialized data is saved in Users/"+currentUser+"/"+this.currentUser+".data");
                names.add(obj.AlbumName);

                albumListView.setItems(names);

            }catch(IOException i) {
                i.printStackTrace();
            }

        }
    }

    /**
     * Handles button for opening the album. Opens the album goes to photoView and loads photos from photoview.
     */
    @FXML
    protected void openButtonEvent() {
        //Open photo view from here
        if(albumListView.getItems().size() > 0) {
            try {
                FXMLLoader photoPage = new FXMLLoader(getClass().getResource("/resources/photosPage.fxml"));
                Parent photoParent = (Parent)photoPage.load();
                Scene photoPageScene = new Scene(photoParent);

                //set album path to the path of the album to make it easier in photoController
                photoPageController photoController = new photoPageController();

                AlbumList albumObject = deserialize();
                System.out.println(albumObject.AlbumName);
                while(true) {
                    albumObject = deserialize();
                    if(albumObject.AlbumName.compareTo(albumListView.getSelectionModel().getSelectedItem().toString()) == 0) {
                        photoController.setAlbumPath(albumObject.AlbumPath);
                        break;
                    }
                }


                //parentStage.hide();
                ((Stage)openButton.getScene().getWindow()).setTitle("Album View");
                ((Stage)openButton.getScene().getWindow()).setScene(photoPageScene);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            //No album selected
            AlbumList deserializedObject;
            System.out.println("ERROR: No album selected.");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("No Album Selected");
            alert.setContentText("Nothing to open. No album selected.");
        }



    }

    /**
     * Gets .data file of the user, opens serializable and gets album path. Then, calls delete directory method which
     * deletes all the files within the album as well as the directory itself and removes it from listview.
     */
    @FXML
    protected void deleteButtonEvent() {
        System.out.println(albumListView.getSelectionModel().getSelectedItem().toString());

        AlbumList e = null;



        try {
            FileInputStream fileIn = new FileInputStream("Users/"+currentUser+"/"+this.currentUser+".data");
            ObjectInputStream in = new ObjectInputStream(fileIn);

                e = (AlbumList) in.readObject();

                if (e.AlbumName.compareTo(albumListView.getSelectionModel().getSelectedItem().toString()) == 0) {
                    File dir = new File(e.AlbumPath);
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("ARE YOU SURE?");
                    alert.setHeaderText("Are you sure you want to delete the selected folder?");
                    alert.setContentText("Deleting the folder will remove ALL files in it. Make sure the folder " +
                            "you have selected contains only the album pictures");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK){
                        // ... user chose OK
                        deleteDirectory(dir);
                        albumListView.getItems().remove(albumListView.getSelectionModel().getSelectedIndex());
                    } else {
                        // ... user chose CANCEL or closed the dialog
                        System.out.println("CANCEL");
                    }

                    in.close();
                    fileIn.close();
                }


        }catch(IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            c.printStackTrace();
        }


    }

    /**
     * Handles logging out of the username and showing main login screen and saves updates.
     */
    @FXML
    protected void logoutAction() {
        System.out.println("Handle Logout Event");
        System.out.println("CurrentUser: " + currentUser);
        //Save new album path in serialized object.
        File yourFile = new File("Users/"+this.currentUser+"/"+this.currentUser+".data");
        try{
            yourFile.createNewFile(); // if file already exists will do nothing
            FileOutputStream oFile = new FileOutputStream(yourFile, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            AlbumList temp = obj;

            FileOutputStream fileOut =
                    new FileOutputStream("Users/"+currentUser+"/"+this.currentUser+".data");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            while(temp != null) {
                out.writeObject(temp);
                temp = temp.next;
            }

            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in Users/"+currentUser+"/"+this.currentUser+".data");

        }catch(IOException i) {
            i.printStackTrace();
        }
        logoutActionEvent((Stage)logoutButton.getScene().getWindow());
    }


    /**
     * Initial method to start new scene.
     * @param loginPage
     */
    @FXML
    @Override
    public void start(Stage loginPage) {
        loginStage = loginPage;
        System.out.println("Start method in albumViewController " + currentUser);
        //Load albums in the list.
        loadList();
    }


    /**
     * Handles action event for logging out of the program.
     */
    public void logoutActionEvent(Stage loginPage) {
        //Save serialized file first.

        try{
            Main m = new Main();
            m.start(loginPage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads listView with current albums.
     */
    public void loadList() {
        AlbumList e = null;
        try {
            FileInputStream fileIn = new FileInputStream("Users/"+currentUser+"/"+currentUser+".data");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            int i = 0;
            e = (AlbumList) in.readObject();
            if(!e.AlbumName.isEmpty()) {
                names.add(e.AlbumName);
            }
            in.close();
            fileIn.close();
        }catch(IOException i) {
            System.out.println("Nothing to load");
            i.printStackTrace();
            return;
        }catch(ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return;
        }
    }

    /**
     * !!MAKE SURE YOU ARE SURE WHAT YOU WANT TO DELETE OR IT WILL BE DELETED FROM COMPUTER WITH ALL THE FILES NESTED INSIDE THE DIRECTORY!!
     * Deletes directory of the selected Album from the computer.
     * @param directory
     * @return
     */
    public static boolean deleteDirectory(File directory) {

        if(directory.exists()){
            File[] files = directory.listFiles();
            if(null!=files){
                for(int i=0; i<files.length; i++) {
                    if(files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    }
                    else {
                        files[i].delete();
                    }
                }
            }
        }
        return(directory.delete());

    }

    private AlbumList deserialize() {
        AlbumList e = null;
        try {
            FileInputStream fileIn = new FileInputStream("Users/"+this.currentUser+"/"+this.currentUser+".data");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (AlbumList) in.readObject();
            in.close();
            fileIn.close();
            return e;
        }catch(IOException i) {
            i.printStackTrace();
            return null;
        }catch(ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return null;
        }
    }

    private void serialize(AlbumList first) {

        File yourFile = new File("Users/"+this.currentUser+"/"+this.currentUser+".data");
        while(first != null) {
            try{
                yourFile.createNewFile(); // if file already exists will do nothing
                FileOutputStream oFile = new FileOutputStream(yourFile, false);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                FileOutputStream fileOut = new FileOutputStream("Users/"+currentUser+"/"+this.currentUser+".data");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(first);
                out.close();
                fileOut.close();
                System.out.printf("Serialized data is saved in Users/"+currentUser+"/"+this.currentUser+".data");
                first = first.next;
            }catch(IOException i) {
                i.printStackTrace();
            }
        }


    }


}
